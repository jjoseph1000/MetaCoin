import 'zeppelin-solidity/contracts/ownership/Ownable.sol';
import 'zeppelin-solidity/contracts/mocks/ReentrancyAttack.sol';
import 'zeppelin-solidity/contracts/mocks/ReentrancyMock.sol';

contract MyContract is Ownable, ReentrancyAttack, ReentrancyMock {

}