pragma solidity ^0.4.19;

/**
 * @title Reentrancy
 * @dev This contract simply lets people store their funds in a bank and withdraw at any time.
**/
contract ReentrancyFix {
    mapping (address => uint256) public balances;
    bool transferInProgress;

    function ReentrancyFix() {
        transferInProgress = false;
    }

    modifier preventRecursion() {
        if(transferInProgress == false) {
            transferInProgress = true;

            _;

            transferInProgress = false;
        }
    }


    /**
     * @dev Withdraw allows the sender to withdraw their full balance.
    **/
    function withdraw()
      external preventRecursion
    {      
        require(balances[msg.sender] > 0);
         
        if (msg.sender.send(balances[msg.sender]))
        {
            balances[msg.sender] = 0;
        }
    }
    
    /**
     * @dev Used by individuals to deposit funds into the bank.
    **/
    function deposit()
      external
      payable
    {
        balances[msg.sender] += msg.value;
    }
    
    /**
     * @dev Check the balance of any address.
    **/
    function balanceOf(address _owner)
      external
    returns (uint256 balance)
    {
        balance = balances[_owner];
    }
}

