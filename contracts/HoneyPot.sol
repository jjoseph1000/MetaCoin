pragma solidity ^0.4.17;
contract HoneyPot {
  mapping (address => uint) public balances;
  function HoneyPot() payable {
    put();
  }
  function put() payable {
    balances[msg.sender] = msg.value;
  }
  function get() {
    if (!msg.sender.call.value(balances[msg.sender])()) {
      throw;
    }
      balances[msg.sender] = 0;
  }

  function see() view returns (uint256, uint256){
      return (balances[msg.sender],this.balance);
  }
  function() {
    throw;
  }
}