pragma solidity ^0.4.17;
contract Alpha {
    struct Beta {
        uint test;
    }
    Beta public beta;
    function test()
        public
        returns (uint betaStor, uint betaMem, uint simple) {
        beta.test = 1;
        storageSet(beta);
        set(beta);        
        Beta memory intMem = beta;
        set(intMem);
        uint testInt = 1;
        setInt(testInt);
        return (beta.test, intMem.test, testInt);
    }
    function storageSet(Beta storage d)
        internal {
        d.test = 2;
    }
    function set(Beta d)
        internal
        pure {
        d.test = 3;
    }
    function setInt(uint i)
        internal
        pure {
        i = 4;
    }
}