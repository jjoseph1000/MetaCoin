pragma solidity ^0.4.19;

/**
 * @title King of the Hill
 * @dev A user pays the king the "attackPrice" to take over the
 * @dev hill for 1 more Ether than that king paid.
**/
contract Koth {
    address public king;
    uint256 public attackPrice;
    
    function Koth() 
      public
    {
        king = msg.sender;
        attackPrice = 1 ether;
    }
    
    /**
     * @dev Pay to take the hill from the current king.
    **/
    function attackHill() 
      external
      payable
    {
        require(msg.value >= attackPrice);
        
        king.transfer(msg.value);
        king = msg.sender;
        
        attackPrice += 1 ether;
    }
}
