pragma solidity ^0.4.19;

/**
 * @title King of the Hill
 * @dev A user pays the king the "attackPrice" to take over the
 * @dev hill for 1 more Ether than that king paid.
**/
contract KothFix {
    address public king;
    uint256 public attackPrice;
    mapping (address => uint256) unclaimedFunds;
    bool public transferInProgress;

    function KothFix() 
      public
    {
        king = msg.sender;
        attackPrice = 1 ether;
        transferInProgress = false;
    }

        modifier preventRecursion() {
        if(transferInProgress == false) {
            transferInProgress = true;

            _;

            transferInProgress = false;
        }
    }
    
    /**
     * @dev Pay to take the hill from the current king.
    **/
    function attackHill() 
      external
      payable
    {
        require(msg.value >= attackPrice);
        
        unclaimedFunds[king] += msg.value;
        king = msg.sender;
        
        attackPrice += 1 ether;
    }

     /**
     * @dev Separate call to pull funds after dethronement.
    **/
    function claimFunds() external preventRecursion
    {
        require(unclaimedFunds[msg.sender] > 0);

        if (msg.sender.send(unclaimedFunds[msg.sender]))
        {
            unclaimedFunds[msg.sender] = 0;
        }

    }

    function getUnclaimedFundsBalanceOf(address user) external view returns (uint256)  {
        return (unclaimedFunds[user]);
    }

}
